import React, {useState} from 'react';
import './App.css';
import './MyStyles.css';

function App() {
  const [status, setStatus] = useState(null);

  const url = 'http://localhost:8085/all';
  const options = {
    method: 'GET',
  };

  const test = async() => {
    let resp = {};
    try {
      resp = await fetch(url, options);
    } catch (error) {
      setStatus(500);
    }
    setStatus(resp.status);
    console.log(resp);
  }

  const message = () => {
    if (!status) {
      return (
        <h3 className="orange">Вы еще не протестировали сервер!</h3>
      )
    }
    if (status === 200) {
      return (
        <h3 className="green">Сервер работает!</h3>
      )
    }
    return (
      <h3 className="red">Проверьте сервер! Возможно, его задели лапкой!</h3>
    )
  }

  return (
    <div className="App">
      <button className="large green button" onClick={test}>Протестировать</button>
      <div className="logs">
        {message()}
      </div>

    </div>
  );
}

export default App;
