package com.example.server;

import org.springframework.data.repository.CrudRepository;

import com.example.server.Weather;


public interface WeatherRepository extends CrudRepository<Weather, Integer> {
}
